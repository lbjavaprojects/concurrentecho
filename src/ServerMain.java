import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerMain {
  public static void main(String ... args) throws IOException{
	  try(ServerSocket ss=new ServerSocket(8189)){
		  while(true){
			  Socket s=ss.accept();
			  EchoTask et=new EchoTask(s);
			  Thread t=new Thread(et);
			  t.start();
		  }
	  }
  }
}
