import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class EchoTask implements Runnable{
	private Socket s;
	private static int connectionCount=0;
	public EchoTask(Socket s) {
		this.s=s;
		connectionCount++;
	}
	@Override
	public void run() {
		try(InputStream is=s.getInputStream();
				OutputStream os=s.getOutputStream();
				PrintWriter pw=new PrintWriter(os);
				Scanner sc=new Scanner(is)){
			pw.println("Jesteś "+connectionCount+" użytkownikiem na serwerze.");
			printMonit(pw);
			boolean sessionFinished=false;
			while(sc.hasNextLine() && !sessionFinished){
				String nextLine=sc.nextLine();
				switch(nextLine){
				case "stop":
					pw.println("Serwer kończy działanie.");
					pw.flush();
					System.exit(0);
				case "count":
					pw.println("Liczna użytkowników: "+connectionCount);
					printMonit(pw);
					break;
				case "quit":
					sessionFinished=true;
					connectionCount--;
					pw.println("Koniec sesji użytkownika.");
					pw.flush();
					break;
				default:
					pw.println("echo: "+nextLine);
					printMonit(pw);
				}

			}
			connectionCount--;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void printMonit(PrintWriter pw){
		pw.print(">");
		pw.flush();
	}

}
